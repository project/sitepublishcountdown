
DESCRIPTION
-----------

 * Site Publish Countdown allows to you to redirect all anonymous users to a countdown page until your website will be publised.
 * You can set a publish date/time, before that moment the countdonw page will be showed, after, the site will be available for all users



REQUIREMENTS
------------

 * This module depends on *libraries* and *variable* modules
 * You must to download jquery.countdown (http://kemar.github.io/jquery.countdown/) to libraries folder
   


INSTALLATION
------------

 1. CREATE DIRECTORY

    Create a new directory "sitepublishcountdown" in the sites/all/modules directory and
    place the entire contents of this module folder in it.

 2. ENABLE THE MODULE

    Enable the module on the Modules admin page.

 3. LIBRARIES

    Download jquery.countdown (http://kemar.github.io/jquery.countdown/) to sites/all/libraries/jquery.countdown

 4. CONFIGURE SITE PUBLISH COUNTDOWN

    Configure this module on the Site Publish Countdown admin pages:
      Configuration > System > Site Publish Countdown

 5. SITE PUBLISH COUNTDOWN THEMING

    More control over the content of countdonw page can be achieved using Drupal theming. 
    Theme your countdown page by copying
    sitepublishcountdown.tpl.php into your theme directory and edit the content.
    The file is self documented listing all available variables.
